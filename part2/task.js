var starter = {
    getEvenNumbersSum: getEvenNumbersSum,
    checkIsEven: checkIsEven,
    getNaturalNumberRoot: getNaturalNumberRoot,
    getNaturalNumberRootBinary: getNaturalNumberRootBinary,
    getFactorial: getFactorial,
    getDigitsSum: getDigitsSum,
    reversNumber: reversNumber

}

module.exports = starter;

function getEvenNumbersSum() {
    var result = [];
    var sum = 0;
    var evenNumersAmount = 0;
    for (var i = 1; i < 100; i++) {
        if (i % 2 === 0) {
            sum = sum + i;
            evenNumersAmount++;
        }
    }
    result [0] = sum;
    result [1] = evenNumersAmount;
    return result;
}

function checkIsEven(value) {

    if (value < 2 || value % 2 === 0 && value !== 2) {
        return false;
    }

    return true;
}

function getNaturalNumberRoot(number) {

    if (number === 0 || number === 1) {
        return number;
    }

    var i = 1, result = 1;

    while (result <= number) {
        i++;
        result = i * i;
    }

    return i - 1;
}

function getNaturalNumberRootBinary(number) {

    var start = 0, result = number;
    while (start <= result) {
        var temp = (start + result) / 2;
        var middle = Math.floor(temp);

        if (middle * middle > number) {
            result = middle - 1;
        } else {
            start = middle + 1;
        }
    }
    return result;
}

function getFactorial(n) {
    if (n === 1) {
        return 1;
    } else {
        return n * getFactorial(n - 1);
    }
}

function getDigitsSum(number) {
    var sum = 0, tmp;
    while (number) {
        tmp = number % 10;
        number = (number - tmp) / 10;
        sum += tmp;
    }
    return sum;
}

function reversNumber(number) {
    var tmp;
    var result = 0;
    while (number) {
        tmp = number % 10;
        number = (number - tmp) / 10;
        result *= 10;
        result += tmp;
    }
    return result;
}



