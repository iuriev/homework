var assert = require('assert');
var starter = require('./task.js');

describe('getEvenNumbersSum', function () {
    it('should return [2450,49] when range from 0 to 99', function () {
        assert.deepEqual([2450, 49], starter.getEvenNumbersSum());
    });
});

describe('checkIsEven', function () {
    it('should return true when parametr is 3', function () {
        assert.equal(true, starter.checkIsEven(3));
    });
    it('should return true when parametr is 5', function () {
        assert.equal(true, starter.checkIsEven(5));
    });
    it('should return false when parametr is 12', function () {
        assert.equal(false, starter.checkIsEven(12));
    });
    it('should return false when parametr is 1', function () {
        assert.equal(false, starter.checkIsEven(1));
    });
    it('should return false when parametr is 113', function () {
        assert.equal(true, starter.checkIsEven(113));
    });
});


describe('getNaturalNumberRoot', function () {
    it('should return 31 when parametr is 1000', function () {
        assert.equal(31, starter.getNaturalNumberRoot(1000));
    });
    it('should return 0 when parametr is 0', function () {
        assert.equal(0, starter.getNaturalNumberRoot(0));
    });
    it('should return 6 when parametr is 36', function () {
        assert.equal(6, starter.getNaturalNumberRoot(36));
    });
});

describe('getNaturalNumberRootBinary', function () {
    it('should return 31 when parametr is 1000', function () {
        assert.equal(31, starter.getNaturalNumberRootBinary(1000));
    });
    it('should return 0 when parametr is 0', function () {
        assert.equal(0, starter.getNaturalNumberRootBinary(0));
    });
    it('should return 6 when parametr is 36', function () {
        assert.equal(6, starter.getNaturalNumberRootBinary(36));
    });
});

describe('getFactorial', function () {
    it('should return 1 when parametr is 1', function () {
        assert.equal(1, starter.getFactorial(1));
    });
    it('should return 24 when parametr is 4', function () {
        assert.equal(24, starter.getFactorial(4));
    });
    it('should return 40320 when parametr is 8', function () {
        assert.equal(40320, starter.getFactorial(8));
    });
});

describe('getDigitsSum', function () {
    it('should return 1 when parametr is 1', function () {
        assert.equal(1, starter.getDigitsSum(1));
    });
    it('should return 8 when parametr is 44', function () {
        assert.equal(8, starter.getDigitsSum(44));
    });
    it('should return 0 when parametr is 0', function () {
        assert.equal(0, starter.getDigitsSum(0));
    });
    it('should return -6 when parametr is -123', function () {
        assert.equal(-6, starter.getDigitsSum(-123));
    });
});

describe('reversNumber', function () {
    it('should return 1 when parametr is 1', function () {
        assert.equal(1, starter.reversNumber(1));
    });
    it('should return 21 when parametr is 12', function () {
        assert.equal(21, starter.reversNumber(12));
    });
    it('should return 321 when parametr is 123', function () {
        assert.equal(321, starter.reversNumber(123));
    });
});
