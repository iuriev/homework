var assert = require('assert');
var starter = require('./task.js');

describe('getDayName', function() {
    it('should return Monday when param is 1', function(){
        assert.equal("Monday", starter.getDayName(1));
    });
});

describe('countDistance', function() {
    it('should return 5 when params are(x1 = 0,y1 = 0,x2 = 3, y2 = 4)', function(){
        assert.equal(5, starter.countDistance(0,0,3,4));
    });
});


describe('convertNumberToText', function() {
    it('should return "one hundred twenty three" when param is 123', function(){
        assert.equal("one hundred twenty three", starter.convertNumberToText(123));
    });
    it('should return "twelve" when param is 12', function(){
        assert.equal("twelve", starter.convertNumberToText(12));
    });
    it('should return "nine hundred ninety nine" when param is 999', function(){
        assert.equal("nine hundred ninety nine", starter.convertNumberToText(999));
    });
    it('should return "zero" when param is 0', function(){
        assert.equal("zero", starter.convertNumberToText(0));
    });
    it('should return "to large number" when param is 99999', function(){
        assert.equal("to large number", starter.convertNumberToText(99999));
    });

    it('should return "one hundred" when param is 100', function(){
        assert.equal("one hundred", starter.convertNumberToText(100));
    });



});

describe('convertTextToNumber', function() {
    it('should return "one hundred twenty three" when param is 123', function(){
        assert.equal(123, starter.convertTextToNumber("one hundred twenty three"));
    });
    it('should return "eleven" when param is 11', function(){
        assert.equal(11, starter.convertTextToNumber("eleven"));
    });
    it('should return "zero" when param is 0', function(){
        assert.equal(0, starter.convertTextToNumber("zero"));
    });
    it('should return "nine hundred ninety nine" when param is 999', function(){
        assert.equal(999, starter.convertTextToNumber("nine hundred ninety nine"));
    });

});
