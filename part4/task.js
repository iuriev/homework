var starter = {
    getDayName: getDayName,
    countDistance: countDistance,
    convertNumberToText: convertNumberToText,
    convertTextToNumber: convertTextToNumber
}

module.exports = starter;
var helper = require('./helper.js');

function getDayName(number) {
    var weekday = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    return weekday[number - 1]
}

function countDistance(x1, y1, x2, y2) {
    return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
}

function convertNumberToText(number) {
    var units = new Array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen");
    var tens = new Array("twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety");
    var resultString = "";

    if (number > 999) {
        return "to large number";
    }

    if (number === 0) {
        return units[0];
    }

    for (var i = 9; i >= 1; i--) {
        if (number >= i * 100) {
            resultString = resultString + units[i];
            resultString = resultString + " hundred";
            number = number - i * 100;
            i = 0;
        }
    }

    for (var i = 9; i >= 2; i--) {
        if (number >= i * 10) {
            resultString = resultString + " ";
            resultString = resultString + tens[i - 2];
            number = number - i * 10;
            i = 0;
        }
    }

    for (var i = 1; i < 20; i++) {
        if (number === i) {
            if(resultString !== "")
            {
                resultString = resultString + " ";
            }
            resultString = resultString + units[i];
        }
    }
    return resultString;
}

function convertTextToNumber(string) {

    var array = string.toString().split(" ");
    var number = 0;
    for (var i = 0; i < array.length; i++) {
        number = helper.checkWord(array[i], number);
    }
    return number;
}
