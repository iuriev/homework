var assert = require('assert');
var starter = require('./task.js');

describe('getMinElementInArray', function () {
    it('should return -188 when array is [1,2,30,5,-188,-10,100]', function () {
        assert.equal(-188, starter.getMinElementInArray([1, 2, 30, 5, -100, -10, 100, -188, 77, 654, 1]));
    });
});

describe('getMaxElementInArray', function () {
    it('should return 654 when array is [1,2,30,5,654,-10,100]', function () {
        assert.equal(654, starter.getMaxElementInArray([1, 2, 30, 5, 654, -10, 100]));
    });
});

describe('getMinIndex', function () {
    it('should return 2 when array is [1,2,-188, 77,654, 1]', function () {
        assert.equal(2, starter.getMinIndex([1, 2, -188, 77, 654, 1]));
    });
});

describe('getMaxIndex', function () {
    it('should return 4 when array is [1,2,-188, 77,654, 1]', function () {
        assert.equal(4, starter.getMaxIndex([1, 2, -188, 77, 654, 1]));
    });
});

describe('getOddIndexElementsSum', function () {
    it('should return 80 when array is [1,2,-188, 77,654, 1]', function () {
        assert.equal(80, starter.getOddIndexElementsSum([1, 2, -188, 77, 654, 1]));
    });
});

describe('reverseArray', function () {
    it('should return [6, 5, 4, 3, 2, 1] when array is [1, 2, 3, 4, 5, 6]', function () {
        assert.deepEqual([1, 2, 3, 4, 5, 6], starter.reverseArray([6, 5, 4, 3, 2, 1]));
    });
});

describe('getOddElementsCounter', function () {
    it('should return 3 when array is [6,5,4,3,2,1] ', function () {
        assert.equal(3, starter.getOddElementsCounter([6, 5, 4, 3, 2, 1]));
    });

});

describe('swapArrayParts ', function () {
    it('should return [3,2,1,6,5,4] when array is [6,5,4,3,2,1]', function () {
        assert.deepEqual([3, 2, 1, 6, 5, 4], starter.swapArrayParts([6, 5, 4, 3, 2, 1]));
    });
    it('should return [3,2,1,5,6,5,4] when array is [6,5,4,5,3,2,1]', function () {
        assert.deepEqual([3, 2, 1,5, 6, 5, 4], starter.swapArrayParts([6, 5, 4, 5, 3, 2, 1]));
    });
});

describe('bubbleSort', function () {
    it('should return [1, 2, 3, 4, 5, 6] when array is [6,5,4,3,2,1]', function () {
        assert.deepEqual([1, 2, 3, 4, 5, 6], starter.bubbleSort([6, 5, 4, 3, 2, 1]));
    });
    it('should return [1, 2, 3, 4, 5, 6] when array is [6,5,4,3,2,1]', function () {
        assert.deepEqual([1, 2, 3, 4, 5, 6], starter.bubbleSort([6, 5, 4, 3, 2, 1]));
    });
    it('should return [1, 2, 3, 4, 5, 6] when array is [6,5,4,3,2,1]', function () {
        assert.deepEqual([1, 2, 4, 4, 5, 6], starter.bubbleSort([6, 5, 4, 4, 2, 1]));
    });
});

describe('insertSort', function () {
    it('should return [1, 2, 3, 4, 5, 6] when array is [6,5,4,3,2,1]', function () {
        assert.deepEqual([1, 2, 3, 4, 5, 6], starter.insertSort([6, 5, 4, 3, 2, 1]));
    });
});

describe('selectionSort', function () {
    it('should return [1, 2, 3, 4, 5, 6] when array is [6,5,4,3,2,1]', function () {
        assert.deepEqual([1, 2, 3, 4, 5, 6], starter.selectionSort([6, 5, 4, 3, 2, 1]));
    });
    it('should return [1, 2, 3, 4, 5, 6, 7] when array is [7, 6,5,4,3,2,1]', function () {
        assert.deepEqual([1, 2, 3, 4, 5, 6, 7], starter.selectionSort([7, 6, 5, 4, 3, 2, 1]));
    });
    it('should return [1, 2, 3, 4, 5, 6, 7] when array is [7, 6,5,4,3,2,1]', function () {
        assert.deepEqual([1, 2, 4, 4, 5, 6, 7], starter.selectionSort([7, 6, 5, 4,4, 2, 1]));
    });
});