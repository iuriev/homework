var starter = {
    getMinElementInArray: getMinElementInArray,
    getMaxElementInArray: getMaxElementInArray,
    getMinIndex: getMinIndex,
    getMaxIndex: getMaxIndex,
    getOddIndexElementsSum: getOddIndexElementsSum,
    reverseArray: reverseArray,
    getOddElementsCounter: getOddElementsCounter,
    swapArrayParts: swapArrayParts,
    bubbleSort: bubbleSort,
    insertSort: insertSort,
    selectionSort: selectionSort
}

module.exports = starter;

function getMinElementInArray(arr) {

    var min = arr[0];
    for (var i = 1; i < arr.length; i++) {

        if (arr[i] < min) {
            min = arr[i];
        }
    }
    return min;
}

function getMaxElementInArray(arr) {
    var max = arr[0];
    for (var i = 1; i < arr.length; i++) {

        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return max;
}

function getMinIndex(arr) {
    var minIndex = 0;
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] < arr[minIndex]) {
            minIndex = i;
        }
    }
    return minIndex;
}

function getMaxIndex(arr) {
    var maxIndex = 0;
    for (var i = 1; i < arr.length; i++) {
        if (arr[i] > arr[maxIndex]) {
            maxIndex = i;
        }
    }
    return maxIndex;
}

function getOddIndexElementsSum(arr) {
    var oddIndexSum = 0;
    for (var i = 1; i < arr.length; i = i + 2) {
        oddIndexSum = oddIndexSum + arr[i];
    }
    return oddIndexSum;
}

function reverseArray(arr) {
    var res = [];
    for (var i = arr.length - 1; i >= 0; i--) {
        res.push(arr[i]);
    }
    return res;
}

function getOddElementsCounter(arr) {
    var oddElements = 0;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] % 2 !== 0) {
            oddElements++;
        }
    }
    return oddElements;
}

function swapArrayParts(arr) {
    n = arr.length;

    if (n % 2 === 0) {
        for (var i = 0; i < n / 2; i++) {
            var t = arr[n / 2 + i];
            arr[n / 2 + i] = arr[i];
            arr[i] = t;
        }
        return arr;
    } else {
        var midddleIndex = Math.floor(arr.length / 2);
        var midValue = arr[midddleIndex];
        arr.splice(midddleIndex, 1);
        n = arr.length;
        for (var i = 0; i < n / 2; i++) {
            var t = arr[n / 2 + i];
            arr[n / 2 + i] = arr[i];
            arr[i] = t;
        }
        arr.splice(midddleIndex, 0, midValue);
        return arr;
    }
}

function bubbleSort(arr) {
    for (var j = arr.length - 1; j > 0; j--) {
        for (var i = 0; i < j; i++) {
            if (arr[i] > arr[i + 1]) {
                var temp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = temp;
            }
        }
    }
    return arr;
}

function selectionSort(arr) {
    var temp = 0;
    for (var i = 0; i < arr.length; ++i) {
        for (var j = i + 1; j < arr.length; ++j) {
            if (arr[i] > arr[j]) {
                temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
    return arr;
}

function insertSort(arr) {
    var length = arr.length;
    for (var i = 1; i < length; i++) {
        var key = arr[i];
        var j = i - 1;
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
    return arr;
}
