var starter = {
    evenCounter: evenCounter,
    getQuarterByCoordinates: getQuarterByCoordinates,
    getPositiveNumbersSum: getPositiveNumbersSum,
    countExpression: countExpression,
    getStudentGrade: getStudentGrade
};
module.exports = starter;

function evenCounter(a, b) {

    if (a % 2 === 0) {
        return a * b;
    }
    return a + b;
}

function getQuarterByCoordinates(x, y) {

    if (x > 0 && y > 0) {
        return 1;
    }
    if (x < 0 && y > 0) {
        return 2;
    }
    if (x < 0 && y < 0) {
        return 3;
    }
    if (x > 0 && y < 0) {
        return 4;
    }
    if (x === 0 && y !== 0) {
        return "Х";
    }
    if (x !== 0 && y === 0) {
        return "Y";
    }
    else {
        return "XY";
    }

}

function getPositiveNumbersSum(a, b, c) {

    var result = 0;

    if (a > 0) {
        result += a;
    }

    if (b > 0) {
        result += b;
    }

    if (c > 0) {
        result += c;
    }
    return result;
}

function countExpression(a, b, c) {

    var composition = a * b * c;
    var sum = a + b + c;
    if(composition > sum){
        return composition + 3;
    }
    else {
        return sum + 3
    }
}

function getStudentGrade(rate) {
    var result = '';

    if (rate < 0 || rate > 100) {
        return "out of range";
    }

    if (rate >= 0 && rate < 20) {
        result = "F";
    } else if (rate < 40) {
        result = "E";
    } else if (rate < 60) {
        result = "D";
    } else if (rate < 75) {
        result = "C";
    } else if (rate < 90) {
        result = "B";
    } else {
        result = "A";
    }
    return result;
}

