var assert = require("chai").assert;
var starter = require('./task.js');

describe('evenCounter', function () {
    it('should return 0 when parameters are (0, 0)', function () {
        assert.equal(0, starter.evenCounter(0, 0));
    });
    it('should return 1 when parameters are (1, 0)', function () {
        assert.equal(1, starter.evenCounter(1, 0));
    });
    it('should return 4 when parameters are (3, 1)', function () {
        assert.equal(4, starter.evenCounter(3, 1));
    });
    it('should return 8 when parameters are (4, 2)', function () {
        assert.equal(8, starter.evenCounter(4, 2));
    });
});

describe('getQuarterByCoordinates', function () {
    it('should return first quarter when parameters equal to (2, 2)', function () {
        assert.equal(1, starter.getQuarterByCoordinates(2, 2));
    });
    it('should return second quarter when parameters equal to (-2, 2)', function () {
        assert.equal(2, starter.getQuarterByCoordinates(-2, 2));
    });
    it('should return third quarter when parameters equal to (-2, -2)', function () {
        assert.equal(3, starter.getQuarterByCoordinates(-2, -2));
    });
    it('should return fotrh quarter when parameters equal to (2, -2)', function () {
        assert.equal(4, starter.getQuarterByCoordinates(2, -2));
    });
    it('should return XY  when parameters equal to (0, 0)', function () {
        assert.equal("XY", starter.getQuarterByCoordinates(0, 0));
    });
    it('should return X  when parameters equal to (0, 2)', function () {
        assert.equal("Х", starter.getQuarterByCoordinates(0, 2));
    });
    it('should return Y  when parameters equal to (2, 0)', function () {
        assert.equal("Y", starter.getQuarterByCoordinates(2, 0));
    });
    it('should return Y  when parameters equal to (-2, 0)', function () {
        assert.equal("Y", starter.getQuarterByCoordinates(-2, 0));
    });

});

describe('getPositiveNumbersSum', function () {
    it('should return 0 when parameters equal to (-2, -2, -2)', function () {
        assert.equal(0, starter.getPositiveNumbersSum(-2, -2, -2));
    });
    it('should return 6 when parameters equal to (2, 2, 2)', function () {
        assert.equal(6, starter.getPositiveNumbersSum(2, 2, 2));
    });
});

describe('countExpression', function () {
    it('should return -3  when parameters equal to (-2, -2, -2)', function () {
        assert.equal(-3, starter.countExpression(-2, -2, -2));
    });
    it('should return 8  when parameters equal to (2, 2, 2)', function () {
        assert.equal(11, starter.countExpression(2, 2, 2));
    });
    it('should return 3  when parameters equal to (0, 0, 0)', function () {
        assert.equal(3, starter.countExpression(0, 0, 0));
    });
});

describe('getStudentGrade', function () {
    it('should return out of range  when parameter is -2', function () {
        assert.equal('out of range', starter.getStudentGrade(-2));
    });
    it('should return F  when parameter is 0', function () {
        assert.equal('F', starter.getStudentGrade(0));
    });
    it('should return E when parameter is 39', function () {
        assert.equal('E', starter.getStudentGrade(39));
    });
    it('should return D when parameter is 59', function () {
        assert.equal('D', starter.getStudentGrade(59));
    });
    it('should return C  when parameter is 74', function () {
        assert.equal('C', starter.getStudentGrade(74));
    });
    it('should return B when parameter is 89', function () {
        assert.equal('B', starter.getStudentGrade(89));
    });
    it('should return A  when parameter is 100', function () {
        assert.equal('A', starter.getStudentGrade(95));
    });
    it('should return A  when parameter is 100', function () {
        assert.equal('A', starter.getStudentGrade(100));
    });
});